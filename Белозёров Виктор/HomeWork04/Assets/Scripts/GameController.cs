﻿using Assets.Scripts.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Assets.Scripts.com;

public interface IUnitView
{
    void EditImage(Sprite str);
}

public class GameController : MonoBehaviour
{
    [SerializeField]
    private GameObject _errorCoinsBg;

    [SerializeField]
    private Text _coinsText;

    [SerializeField]
    private ShopUnitView _cardUnitShop;
    [SerializeField]
    private InventoryUnitView _cardUnitInventory;

    [SerializeField]
    private GameObject _shopContentTransform;
    [SerializeField]
    private GameObject _inventoryContentTransform;

    private List<Unit> _units = new List<Unit>();

    private Inventory _inventory = new Inventory();

    private void Awake()
    {
        EventManager.AddListener(EventManager.BUY_CLICK, BuyClick);
    }
    void Start()
    {
        _coinsText.text = _inventory.coins.ToString();
        AddUnitsToShop();

    }
    void BuyClick(IData data)
    {
        try
        {
            var eventData = (EventData)data;
            Unit boughtUnit = (Unit)eventData.data;


            if(_inventory.Purchase(boughtUnit))
            {
                _coinsText.text = _inventory.coins.ToString();
                AddUnitsToInventory(boughtUnit);
            }
            else
            {
                _errorCoinsBg.SetActive(true);
            }

            
        }
        catch { }

    }

    private void AddUnitsToShop()
    {
        _units.Add(new ShooterUnit("Стрелок", 100, 60, new Characteristic(2, 2, 1, 1)));
        _units.Add(new JeepUnit("Джип", 350, 300, new Characteristic(7, 3, 2, 5)));
        _units.Add(new TankUnit("Танк", 800, 10, new Characteristic(30, 15, 6, 2)));
        _units.Add(new AirplaneUnit("Самолет", 950, 4, new Characteristic(22, 10, 8, 10)));
        GenerationStatsShop();
    }
    private void GenerationStatsShop()
    {
        foreach (var item in _units)
        {
            ShopUnitView view = Instantiate(_cardUnitShop, _shopContentTransform.transform);
            view.MyInit(item);
            StartCoroutine(LoadPicture(item.img, view));
        }
    }
    private void AddUnitsToInventory(Unit unit)
    {
        InventoryUnitView view = Instantiate(_cardUnitInventory, _inventoryContentTransform.transform);
        view.MyInit(unit);
        StartCoroutine(LoadPicture(unit.img, view));

    }
    IEnumerator LoadPicture(string url, IUnitView vw)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);

        yield return request.SendWebRequest();

        Texture2D webTexture = ((DownloadHandlerTexture)request.downloadHandler).texture as Texture2D;
        Sprite webSprite = Sprite.Create(webTexture, new Rect(0.0f, 0.0f, webTexture.width, webTexture.height), new Vector2(0, 0), 100);

        vw.EditImage(webSprite);

    }
}
