﻿using Assets.Scripts.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUnitView : MonoBehaviour, IUnitView
{
    [SerializeField]
    private Image _unitImg;
    [SerializeField]
    private Text _titleText;
    [SerializeField]
    private Text _descriptionText;

    public void MyInit(Unit unit)
    {
        _titleText.text = unit.title.ToString();
        _descriptionText.text = $"Нападение: {unit.characteristic.attack}\nЗащита: {unit.characteristic.protection}\nСкорость: {unit.characteristic.speed}\nУдача: {unit.characteristic.protection}";
    }
    public void EditImage(Sprite spr)
    {
        _unitImg.sprite = spr;
    }
}
