﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Assets.Scripts.Models;
using Assets.Scripts.com;

class ShopUnitView : MonoBehaviour, IUnitView
{
    [SerializeField]
    private Button _buyButton;

    [SerializeField]
    private Image _unitImg;

    [SerializeField]
    private Text _priceText;

        [SerializeField]
    private Text _titleText;

        [SerializeField]
    private Text _descriptionText;


    private Unit _unitObj;

    void Start()
    {
        _buyButton.onClick.AddListener(BuyButtonClick);
    }

    private void BuyButtonClick()
    {
        EventManager.DispatchEvent(EventManager.BUY_CLICK, new EventData(_unitObj));
    }
    public void MyInit(Unit unit)
    {
        _unitObj = unit;
        _priceText.text = unit.price.ToString();
        _titleText.text = unit.title.ToString();
        _descriptionText.text = $"Нападение: {unit.characteristic.attack}\nЗащита: {unit.characteristic.protection}\nСкорость: {unit.characteristic.speed}\nУдача: {unit.characteristic.protection}";

    }
    public void EditImage(Sprite spr)
    {
        _unitImg.sprite = spr;
    }
}
