﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models
{
    class Inventory
    {
        public List<Unit> units = new List<Unit>();
        public int coins { get; set; }

        public Inventory()
        {
            coins = 10000;
        }

        public bool Purchase(Unit unit)
        {
            if((coins-unit.price)<0)
            {
                return false;
            }
            units.Add(unit);
            coins -= unit.price;

            return true;
        }

    }
}
