﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Models
{
    class ShooterUnit: Unit
    {

        public ShooterUnit(string title, int price, int patrons, Characteristic charact) : base(title, price, patrons, charact)
        {
            img = "https://vignette.wikia.nocookie.net/aow3/images/a/af/F2_inf_light_0002.png/revision/latest?cb=20180626100115&path-prefix=ru";
        }

        public override void Move()
        {
            Debug.Log("Идет...");
        }

        public override void Attack()
        {
            if(patrons>0)
            {
                Debug.Log("Стреляет тра-та-та...");
            }
            else
            {
                Debug.Log("Боеприпасов нету!");
            }
        }
    }
}
