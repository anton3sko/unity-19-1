﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Models
{
    class AirplaneUnit: Unit
    {
        public AirplaneUnit(string title, int price, int patrons, Characteristic charact) : base(title, price, patrons, charact)
        {
            img = "http://aow3.ru/data/uploads/wiki/units-resistance/albatross.png";
        }

        public override void Move()
        {
            Debug.Log("Летит...");
        }

        public override void Attack()
        {
            if (patrons > 0)
            {
                Debug.Log("Ракета в-ш-ш...");
            }
            else
            {
                Debug.Log("Боеприпасов нету!");
            }
        }
    }
}
