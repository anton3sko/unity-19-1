﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models
{
    public class Characteristic
    {
        public int attack { get; set; }
        public int protection { get; set; }
        public int luck { get; set; }
        public int speed { get; set; }

        public Characteristic(int attack, int protection, int luck, int speed)
        {
            this.attack = attack;
            this.protection = protection;
            this.luck = luck;
            this.speed = speed;
        }
        public Characteristic(Characteristic charact)
        {
            attack = charact.attack;
            protection = charact.protection;
            luck = charact.luck;
            speed = charact.luck;
        }

    }
}
