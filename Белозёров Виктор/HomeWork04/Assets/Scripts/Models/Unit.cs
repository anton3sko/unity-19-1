﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models
{
    public class Unit
    {
        public static int Count;

        public int id { get; set; }
        public string title { get; set; }
        public int price { get; set; }
        public int patrons { get; set; }
        public Characteristic characteristic { get; set; }

        public string img { get; set; }

        public Unit(string title, int price, int patrons, Characteristic charact)
        {
            id = ++Count;
            this.title = title;
            this.price = price;
            this.patrons = patrons;
            characteristic = new Characteristic(charact);
        }

        virtual public void Move()
        {

        }

        virtual public void Attack()
        {

        }

    }
}
