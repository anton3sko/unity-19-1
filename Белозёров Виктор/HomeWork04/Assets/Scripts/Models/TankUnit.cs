﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Models
{
    class TankUnit:  Unit
    {

        public TankUnit(string title, int price, int patrons, Characteristic charact) : base(title, price, patrons, charact)
        {
            img = "https://vignette.wikia.nocookie.net/aow3/images/2/22/F1_veh_hammer_0002.png/revision/latest?cb=20180626110206&path-prefix=ru";
        }

        public override void Move()
        {
            Debug.Log("Едет...");
        }

        public override void Attack()
        {
            if (patrons > 0)
            {
                Debug.Log("Стреляет тра-та-та...");
            }
            else
            {
                Debug.Log("Боеприпасов нету!");
            }
        }
        public void AttackShell()
        {
            Debug.Log("Выстрел  ба-бах...");
        }
    }
}
