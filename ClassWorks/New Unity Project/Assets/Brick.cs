﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using com;

public class Brick : MonoBehaviour
{
    [SerializeField]
    private int _points;
    [SerializeField]
    private Image _img;

    private RectTransform _rectTransform;


    [SerializeField]
    //private List<Image> _imgList;
    private Sprite _sprite;

    // Start is called before the first frame update
    void Awake(){
        _rectTransform = GetComponent<RectTransform>();
    }

    void Start()
    {
        //_img.sprite = _sprite;
        //EventManager.AddListener(EventManager.CLICK, OnClick);
    }

    private void OnClick(EventData eventData) {
        _img.sprite = _sprite;
        //_img.color = new Color();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public float Width {
        get {
            return _rectTransform.rect.size.x;
        }
    }

    public float Height {
        get {
            return _rectTransform.rect.size.y;
        }
    }
}
