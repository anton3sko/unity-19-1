﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com;
using UnityEngine.UI;

public class UIPanelController : MonoBehaviour
{
    [SerializeField]
    private Text _txt;
    void Start()
    {
        AddListenners();
    }

    private void AddListenners()
    {
        // add listenner with eventManager on click
        /*EventManager.AddListener(EventManager.CLICK, OnDelEventManager);
        EventManager.AddListener(EventManager.CHANGE_DROPBOX, OnChanGeEventManager);
        EventManager.AddListener(EventManager.CHANGE_DROPBOX, OnChanGeEventManager2);*/
        
    }

    private void OnChanGeEventManager(EventData eventData){
        _txt.text = eventData.Data.ToString();
    }

    private void OnChanGeEventManager2(EventData eventData){
        Debug.Log(eventData.Data.ToString());
    }

    private void OnDelEventManager(EventData eventData) {
        EventDataMouseClick mouseClick = (EventDataMouseClick)eventData;
        Debug.Log(eventData.Data + " -- " + mouseClick.MyInt);
        EventManager.RemoveEvent(EventManager.CLICK, OnDelEventManager);
    }

    
}
