﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.oop.unit;

//namespace com.oop.unit {
public class Main : MonoBehaviour
{
    private List<Unit> _army = new List<Unit>();

    private void CreateArmy() {
        int idCount = 0;
        for(int i = 0; i < 10; i++) {
            idCount++;
            _army.Add(new HeroLand(idCount.ToString()));
        }

        for(int i = 0; i < 10; i++) {
            idCount++;
            _army.Add(new HeroFly(idCount.ToString()));
        }
    }
    
    void Start()
    {
        CreateArmy();
        Unit unit = new Unit("a");
        // unit.Move();

        // HeroLand heroLand = new HeroLand();
        // heroLand.Move();

        //Unit unitA = new HeroLand();
        // Unit unitA = new Unit();
        // unitA.Move();

        // (unitA as HeroLand).GoLegs();

        MoveMyArmy();
    }

    private void MoveMyArmy() {
         for(int i = 0; i < _army.Count; i++) {
            //_army[i].Print();
            _army[i].Move();
        }
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
//}