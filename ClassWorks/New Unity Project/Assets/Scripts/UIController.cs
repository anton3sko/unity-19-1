﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using com;
using TMPro;

public class UIController : MonoBehaviour
{
    [SerializeField]
    private Button _add;
    [SerializeField]
    private Button _del;

    [SerializeField]
    private Dropdown _dropDown;

    TMP_Text _text;

    public List<Color> colors;
    public Color color;

    void Start() 
    {
        AddListenner();
        UnityEngine.Debug.Log(color.ToString());
        UnityEngine.Debug.Log(color.ToString("F5"));
        //OptionData od = new OptionData();
        //_dropDown.AddOptions()
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void AddListenner()
    {
        // add listenner to button
        _add.onClick.AddListener(OnClickAdd);
        //_add.onClick.RemoveAllListeners();
        //_del.onClick.AddListener(OnClickDel);

        _dropDown.onValueChanged.AddListener(OnValueChanged);
    }

    private void OnClickAdd()
    {
        //EventDataMouseClick eventData = new EventDataMouseClick("mouseClick", -1);
        EventManager.DispatchEvent(EventManager.CLICK, new EventDataMouseClick("mouseClick", -1));
    }

    private void OnClickDel()
    {

    }

    private void OnValueChanged(int index) {
        EventData eventData = new EventData(index);
        EventManager.DispatchEvent(EventManager.CHANGE_DROPBOX, eventData);
    }

    void OnDestroy() {
        RemoveListenners();
    }

    private void RemoveListenners() {
        _add.onClick.RemoveListener(OnClickAdd);
        //EventManager.RemoveEvent(EventManager.CLICK, OnDelEventManager);
    }
}
