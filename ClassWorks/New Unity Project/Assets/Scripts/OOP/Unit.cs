﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.oop.unit {
    public class Unit : MonoBehaviour {

        private static int idCount = 100;

        private static int GetId() {
            return idCount++;
        }

        internal UnityIdentity _iden;

        public Unit(string id) {
            _iden = new UnityIdentity(GetId().ToString());
        }

        public Unit(string id, string name) {
            //
        }

        public virtual void Move() {
            UnityEngine.Debug.Log("Unit -> move");
        }

        public void Print() {
            UnityEngine.Debug.Log("UnitId -> " + _iden.Id);
        }

        ~Unit() {
            
        }

        // public UnityIdentity UnityIdentity {
        //     get {
        //         return _iden;
        //     }

        //     set {
        //         _iden = value;
        //     }
        // } 

        // public void SetIden(UnityIdentity id) {
        //     _iden = id;
        // }

        // public UnityIdentity GetIden() {
        //     return _iden;
        // }

    }
}

