﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityIdentity
{
    private string _id;
    private string _name;
    private string _type;

    public UnityIdentity(string id) {
        _id = id;
    }

    public string Id{
        get {
            return _id;
        }
    }
}
