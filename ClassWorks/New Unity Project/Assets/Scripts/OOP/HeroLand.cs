﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.oop.unit {
public class HeroLand : Unit
{
    public HeroLand(string id) : base(id) {

    }

    public override void Move() {
        string id = _iden.Id;
        Debug.Log(id + " :" + "HeroLand --> moveLand");
    }

    public void GoLegs() {
        Debug.Log("HeroLand --> GoLegs");
    }
}
}